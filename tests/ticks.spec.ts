import { describe, expect, it } from "vitest"
import { ticksNb } from "../src/composables/useTicks"

describe("simple number ticks", () => {
	it("simple case", () => {
		expect(ticksNb({ min: 0, max: 5 })).toStrictEqual([0, 5])
	})
})
