import { InjectionKey, computed, reactive, provide } from "vue"
import { scaleDate, unscale as unscl } from "../mods/scale"
import { Axis } from "../lib"
import { PropsDate, kParent } from "../mods/axis"
import { GraphAxis } from "../mods/types"
import { strictInject, isSafeNumber } from "../mods/utils"

export function useAxisDate(props: PropsDate, key: InjectionKey<GraphAxis<Date>>) {
	const axis = strictInject(key)
	const offset = computed(() => axis.offset.value + props.offset)
	const reverse = computed(() => props.reverse)
	const size = computed(() => (isSafeNumber(props.size) ? props.size : axis.size.value - props.offset))
	const values = reactive(new Map<symbol, Date>())

	const min = computed(
		() => props.min ?? new Date(Math.min(...(values.values() as unknown as number[]), 8640000000000000)),
	)
	const max = computed(
		() => props.max ?? new Date(Math.max(...(values.values() as unknown as number[]), -8640000000000000)),
	)
	function scale(value: Date) {
		return scaleDate({
			value,
			min: min.value,
			max: max.value,
			size: size.value,
			offset: offset.value,
			reverse: props.reverse,
		})
	}
	function unscale(value: number) {
		return new Date(
			unscl({
				value,
				min: min.value.getTime(),
				max: max.value.getTime(),
				size: size.value,
				offset: offset.value,
				reverse: props.reverse,
			}),
		)
	}
	function scaleSize(value: Date): never {
		throw new Error("Cannot scale Date in relative size: " + value)
	}

	function setValue(s: symbol, v: unknown) {
		if (v instanceof Date) values.set(s, v)
		else values.delete(s)
	}
	const provided: Axis<Date> = {
		...axis,
		date: true,
		reverse,
		offset,
		size,
		values,
		min,
		max,
		setValue,
		scale,
		unscale,
		scaleSize,
	}
	provide<Axis<Date>>(kParent, provided)
	provide(key, provided)
	return provided
}
