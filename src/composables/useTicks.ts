import { TicksOptionsDate } from "../mods/axis"
import { orderOf } from "../mods/utils"

type NbArgs = { min: number; max: number; nb?: number; pretty?: boolean }
export function ticksNb({ min, max, nb = 2, pretty }: NbArgs): number[] {
	if (max === min) return [min] // return Array(nb).fill(min) // multiples identicals values will dupplicate key v-for
	const n = nb - 1
	if (pretty) {
		const diff = max - min
		const piece = diff / nb
		const order = orderOf(piece)
		const prettyPiece = piece - (piece % order)
		let t = min + prettyPiece - ((min + prettyPiece) % prettyPiece)
		const r: number[] = []
		while (t < max) {
			r.push(t)
			t += prettyPiece
		}
		return r
	}
	if (max < min) return []
	const step = Math.abs((max - min) / n)
	return Array.from({ length: nb }, (_, i) => min + i * step).filter(n => Number.isFinite(n))
}

export function ticksDate(from: Date, to: Date, options?: TicksOptionsDate): Date[] {
	const ticks = []
	const tmp = new Date(from)
	loop: while (tmp < to && tmp >= from) {
		ticks.push(new Date(tmp))
		if (options?.year) tmp.setFullYear(tmp.getFullYear() + options.year)
		if (options?.month) tmp.setMonth(tmp.getMonth() + options.month)
		if (options?.day) tmp.setDate(tmp.getDate() + options.day)
		if (options?.hour) tmp.setHours(tmp.getHours() + options.hour)
		if (options?.minute) tmp.setMinutes(tmp.getMinutes() + options.minute)
		if (options?.second) tmp.setSeconds(tmp.getSeconds() + options.second)
		if (options?.millisecond) tmp.setMilliseconds(tmp.getMilliseconds() + options.millisecond)
		if (tmp.getTime() === from.getTime()) break loop
	}
	ticks.push(to)
	// if (options?.limit) ticks.splice(options.limit)
	return ticks
}
