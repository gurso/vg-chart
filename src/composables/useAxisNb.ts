import { InjectionKey, computed, reactive, provide } from "vue"
import { Axis } from "../lib"
import { PropsAxisNb, kParent } from "../mods/axis"
import { GraphAxis } from "../mods/types"
import { isSafeNumber, cheatLcm, orderOf, strictInject } from "../mods/utils"
import { scale as scl, scaleSize as sclSz, unscale as unscl } from "../mods/scale"

export function useAxisNb(props: PropsAxisNb, key: InjectionKey<GraphAxis<number>>) {
	const axis = strictInject(key)
	const offset = computed(() => axis.offset.value + props.offset)
	const reverse = computed(() => props.reverse)
	const size = computed(() => (isSafeNumber(props.size) ? props.size : axis.size.value - props.offset))
	const values = reactive(new Map<symbol, number>())

	const multiples = computed(() =>
		Array.isArray(props.multiples) ? props.multiples : props.multiples ? [props.multiples] : [],
	)
	const lcm = computed(() =>
		multiples.value.reduce((previous: number, current: number) => cheatLcm(current, previous), 1),
	)
	const _min = computed(() => Math.min(...values.values(), Number.MAX_SAFE_INTEGER))
	const _max = computed(() => Math.max(...values.values(), Number.MIN_SAFE_INTEGER))
	const diff = computed(() => _max.value - _min.value)
	const min = computed(() => {
		if (isSafeNumber(props.min)) return props.min
		let tmp = lcm.value
		if (props.pretty) {
			const order = orderOf(diff.value)
			const m = diff.value < 8 * order ? (diff.value > 2 * order ? order : order / 10) : order * 10
			tmp = cheatLcm(tmp, m)
		}
		if (multiples.value.length || props.pretty) {
			let rest = Math.abs(_min.value % tmp)
			if (_min.value < 0) rest = tmp - rest
			return _min.value - rest
		} else return _min.value
	})
	const max = computed(() => {
		if (isSafeNumber(props.max)) return props.max
		let tmp = lcm.value
		if (props.pretty) {
			const order = orderOf(diff.value)
			const m = diff.value < 8 * order ? (diff.value > 2 * order ? order : order / 10) : order * 10
			tmp = cheatLcm(tmp, m)
		}
		if (multiples.value.length || props.pretty) {
			const rest = Math.abs(_max.value % tmp)
			if (rest === 0) return _max.value
			return _max.value - rest + tmp
		} else return _max.value
	})
	function setValue(s: symbol, v: unknown) {
		if (isSafeNumber(v)) values.set(s, v)
		else values.delete(s)
	}
	function scale(value: number) {
		return scl({
			value,
			min: min.value,
			max: max.value,
			size: size.value,
			offset: offset.value,
			reverse: props.reverse,
		})
	}
	function unscale(value: number) {
		return unscl({
			value,
			min: min.value,
			max: max.value,
			size: size.value,
			offset: offset.value,
			reverse: props.reverse,
		})
	}
	function scaleSize(value: number) {
		return sclSz({
			value,
			min: min.value,
			max: max.value,
			size: size.value,
		})
	}
	const provided: Axis<number> = {
		...axis,
		date: false,
		reverse,
		offset,
		size,
		values,
		multiples,
		min,
		max,
		setValue,
		scale,
		unscale,
		scaleSize,
	}

	provide<Axis<number>>(kParent, provided)
	provide(key, provided)
	return provided
}
