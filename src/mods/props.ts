import { PropType } from "vue"
import { Value } from "./types"

// export const safeValueOrAttr = {
// 	type: [Number, Date, String],
// 	validator(v: number | string | Date) {
// 		return isSafeValue(v) || v.match(/\d+(px|%)?/)
// 	},
// }

export const safeNumber = {
	type: Number,
	validator(v: number): boolean {
		return Number.isFinite(v)
	},
}
export const safeNumberRequired = {
	...safeNumber,
	required: true,
} as const

export const safeNumberDefault0 = {
	...safeNumber,
	default: 0,
}

export const positiveInteger = {
	type: Number,
	validator(v: number): boolean {
		return Number.isInteger(v) && v >= 0
	},
}

export const isSafeValue = (v: unknown): v is Value => Number.isFinite(v) || (v instanceof Date && Number.isFinite(v))
// this version could also replace isSafeNumber ?
// export const isSafeValue = (v: unknown): v is Value => Number.isFinite(v?.valueOf())
export const safeValue = {
	type: [Number, Date] as PropType<Value>,
	validator: isSafeValue,
}
export const safeValueRequired = {
	...safeValue,
	required: true,
} as const

export const d = {
	dx: safeNumberDefault0,
	dy: safeNumberDefault0,
}

export const axis = {
	type: String as PropType<"x" | "y">,
	validator: (v: string): boolean => ["y", "x"].includes(v),
}
