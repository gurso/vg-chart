import { ExtractPropTypes, InjectionKey } from "vue"

// Define props options here to avoid to create a Pie Prop type (typeof below)
export const propsOptions = {
	min: {
		type: Number,
		default: 0,
	},
	max: {
		type: Number,
		default: 100,
	},
	size: {
		type: Number,
		required: true as const,
	},
	hole: {
		type: Number,
		default: 0,
		validator: (n: number) => n >= 0 && n <= 1,
	},
	ply: {
		type: Number,
		validator: (n: number) => n >= 0 && n <= 1,
	},
	gap: {
		type: Number,
		default: 0,
	},
	turn: {
		type: Number,
		default: Math.PI / -2,
	},
}

export const kProps: InjectionKey<Readonly<ExtractPropTypes<typeof propsOptions>>> = Symbol()
