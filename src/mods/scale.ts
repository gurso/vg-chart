import { Padding } from "./types"

export function cssPadding(p: number | number[]): Padding {
	if (Number.isFinite(p)) return [p, p, p, p] as unknown as Padding
	else if (Array.isArray(p)) {
		let r: Padding
		switch (p.length) {
			case 0:
				r = [0, 0, 0, 0]
				break
			case 1:
				r = [p[0]!, p[0]!, p[0]!, p[0]!]
				break
			case 2:
				r = [p[0]!, p[1]!, p[0]!, p[1]!]
				break
			case 3:
				r = [p[0]!, p[1]!, p[2]!, 0]
				break
			default:
				r = [...p] as Padding
		}
		return r
	} else return [0, 0, 0, 0]
}

type Arg<T = number> = {
	max: T
	min: T
	size: number
	value: T
	offset: number
	reverse?: boolean
}

export function scale({ max, min, size, value, offset, reverse }: Arg): number {
	if (max === min) return size / 2
	else {
		const scaled = scaleSize({ value, min, max, size })
		if (reverse) return size - scaled + offset
		return scaled + offset
	}
}

export function scaleDate({ max, min, size, value, offset, reverse }: Arg<Date>): number {
	return scale({ max: max.getTime(), min: min.getTime(), size, value: value.getTime(), offset, reverse })
}

export function scaleSize({
	max,
	min,
	size,
	value,
}: {
	max: number
	min: number
	size: number
	value: number
}): number {
	if (max === min) return 0
	else return ((value - min) / (max - min)) * size
}

export function unscale({ max, min, size, value, offset, reverse }: Arg): number {
	if (reverse) return ((value - offset) / size) * (max - min) - size + min
	return ((value - offset) / size) * (max - min) + min
}
