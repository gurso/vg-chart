import { barryCenter } from "./utils"

/**
 * the purpose to this function is to be given as "type" prop to VgGraphAxisCurve,
 * you shouldn't call it
 * @param point some coord x and y
 * @returns a part of string path
 */
export function linear(point: Readonly<[number, number]>) {
	return `L ${point[0]} ${point[1]}`
}

/**
 * the function create a "type" prop to VgGraphAxisCurve for step curve,
 * you shouldn't call it
 * @param point some coord x and y
 * @returns a function to give as "type" prop to VgGraphAxisCurve
 */
export function stepFactory(align: "center" | "left" | "right" = "center") {
	return function (point: Readonly<[number, number]>, i: number, a: [number, number][]) {
		const bfr = a[i - 1]! // || point // no need ? first point never given as args
		const x = align === "left" ? point[0] : align === "right" ? bfr[0] : bfr[0] + (point[0] - bfr[0]) / 2

		if (i === a.length - 1) {
			return `L ${x} ${bfr[1]} L ${x} ${point[1]} L ${point[0]} ${point[1]}`
		}
		return `L ${x} ${bfr[1]} L ${x} ${point[1]}`
	}
}

function line(pointA: Readonly<[number, number]>, pointB: Readonly<[number, number]>) {
	const lengthX = pointB[0] - pointA[0]
	const lengthY = pointB[1] - pointA[1]
	return {
		length: Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2)),
		angle: Math.atan2(lengthY, lengthX),
	}
}

/**
 * the function create a "type" prop to VgGraphAxisCurve for rounding curve,
 * you shouldn't call it
 * @param smoothing a value to get more or less smooth curve
 * @returns a function to give as "type" prop to VgGraphAxisCurve
 */
export function bumpFactory(smoothing = 2) {
	const controlPoint = (
		current: Readonly<[number, number]>,
		previous: Readonly<[number, number]>,
		next: Readonly<[number, number]>,
		reverse?: Readonly<boolean>,
	) => {
		// When 'current' is the first or last point of the array
		// 'previous' or 'next' don't exist.
		// Replace with 'current'
		const p = previous || current
		const n = next || current
		// Properties of the opposed-line
		const o = line(p, n)
		// If is end-control-point, add PI to the angle to go backward
		const angle = o.angle + (reverse ? Math.PI : 0)
		const length = o.length * smoothing
		// The control point position is relative to the current point
		const x = current[0] + Math.cos(angle) * length
		const y = current[1] + Math.sin(angle) * length
		return [x, y]
	}
	return function (point: Readonly<[number, number]>, i: number, a: [number, number][]) {
		// start control point
		const [cpsX, cpsY] = controlPoint(a[i - 1]!, a[i - 2]!, point)
		// end control point
		const [cpeX, cpeY] = controlPoint(point, a[i - 1]!, a[i + 1]!, true)
		return `C ${cpsX},${cpsY} ${cpeX},${cpeY} ${point[0]},${point[1]}`
	}
}

/**
 * the function create a "type" prop to VgGraphAxisCurve for rounding curve,
 * you shouldn't call it
 * @param fraction a value to get more or less smooth curve
 * @returns a function to give as "type" prop to VgGraphAxisCurve
 */
export function basisFactory(fraction = 0.5) {
	if (fraction < 0 || fraction > 0.5) throw new Error("[basisFactory]: fraction shoub be between 0 and 0.5")
	return (point: Readonly<[number, number]>, i: number, arr: [number, number][]) => {
		if (i === arr.length - 1) return " L " + point.join(" ")

		const p = arr[i - 1]!
		const n = arr[i + 1]!

		const cStart = barryCenter(p, point, fraction)
		const cEnd = barryCenter(point, n, 1 - fraction)

		const aCtrl = barryCenter(cStart, point, fraction)
		const bCtrl = barryCenter(point, cEnd, fraction)

		return ["L", ...cStart, "C", ...aCtrl, ...bCtrl, ...cEnd].join(" ")
	}
}
