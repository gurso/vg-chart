import { InjectionKey, PropType, ExtractPropTypes } from "vue"
import { Axis } from "./types"
import { isSafeNumber } from "./utils"

export const kx: InjectionKey<Axis> = Symbol("Axis X")
export const ky: InjectionKey<Axis> = Symbol("Axis Y")
export const kParent: InjectionKey<Axis> = Symbol("Axis Parent")

export type TicksOptionsDate = {
	year?: number
	month?: number
	day?: number
	hour?: number
	minute?: number
	second?: number
	millisecond?: number
}

export const propsOptionsDate = {
	offset: {
		type: Number,
		default: 0,
		validator(n: number): boolean {
			return isSafeNumber(n) && n >= 0
		},
	},
	size: {
		type: Number,
		validator(n: number): boolean {
			return isSafeNumber(n) && n > 0
		},
	},
	reverse: Boolean,
	ticks: {
		type: Object as PropType<TicksOptionsDate>,
	},
	min: {
		type: Date,
		validator(d: Date) {
			return isSafeNumber(d.valueOf())
		},
	},
	max: {
		type: Date,
		validator(d: Date) {
			return isSafeNumber(d.valueOf())
		},
	},
}
export type PropsDate = ExtractPropTypes<typeof propsOptionsDate>

export const propsOptionsNb = {
	...propsOptionsDate,
	pretty: Boolean,
	min: {
		type: Number,
		validator: isSafeNumber,
	},
	max: {
		type: Number,
		validator: isSafeNumber,
	},
	multiples: {
		type: [Array, Number] as PropType<number[]>,
		default: (): number[] => [],
		validator(arr: number[]): boolean {
			return Array.isArray(arr) ? arr.every(c => Number.isInteger(c) && c > 0) : Number.isInteger(arr) && arr > 0
		},
	},
}
export type PropsAxisNb = Omit<ExtractPropTypes<typeof propsOptionsNb>, "ticks">
