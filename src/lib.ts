import { App, Plugin } from "vue"

import VgGraph from "./components/VgGraph.vue"
import VgGraphGrid from "./components/VgGraphGrid.vue"
import VgGraphScaleX from "./components/VgGraphScaleX.vue"
import VgGraphScaleY from "./components/VgGraphScaleY.vue"
import VgGraphScaleDateX from "./components/VgGraphScaleDateX.vue"
import VgGraphScaleDateY from "./components/VgGraphScaleDateY.vue"
import VgScaledGrid from "./components/VgScaledGrid.vue"
import VgScaledTick from "./components/VgScaledTick.vue"
import VgScaledTicks from "./components/VgScaledTicks.vue"
import VgScaledG from "./components/VgScaledG.vue"
import VgScaledText from "./components/VgScaledText.vue"
import VgScaledValue from "./components/VgScaledValue.vue"
import VgScaledRect from "./components/VgScaledRect.vue"
import VgScaledForeignObject from "./components/VgScaledForeignObject.vue"
import VgScaledSvg from "./components/VgScaledSvg.vue"
import VgScaledCircle from "./components/VgScaledCircle.vue"
import VgScaledEllipse from "./components/VgScaledEllipse.vue"
import VgScaledLine from "./components/VgScaledLine.vue"
import VgScaledCurve from "./components/VgScaledCurve.vue"
import VgPie from "./components/VgPie.vue"
import VgPieArc from "./components/VgPieArc.vue"

export type { Axis, Value, ScaleF, UnscaleF } from "./mods/types"
export { linear, bumpFactory, basisFactory, stepFactory } from "./mods/curve"

export default {
	install(app: App): void {
		app
			.component("VgGraph", VgGraph)
			.component("VgGraphGrid", VgGraphGrid)
			.component("VgGraphScaleX", VgGraphScaleX)
			.component("VgGraphScaleY", VgGraphScaleY)
			.component("VgGraphScaleDateX", VgGraphScaleDateX)
			.component("VgGraphScaleDateY", VgGraphScaleDateY)
			.component("VgScaledGrid", VgScaledGrid)
			.component("VgScaledValue", VgScaledValue)
			.component("VgScaledTick", VgScaledTick)
			.component("VgScaledTicks", VgScaledTicks)
			.component("VgScaledG", VgScaledG)
			.component("VgScaledRect", VgScaledRect)
			.component("VgScaledForeignObject", VgScaledForeignObject)
			.component("VgScaledSvg", VgScaledSvg)
			.component("VgScaledCircle", VgScaledCircle)
			.component("VgScaledEllipse", VgScaledEllipse)
			.component("VgScaledLine", VgScaledLine)
			.component("VgScaledText", VgScaledText)
			.component("VgScaledCurve", VgScaledCurve)
			.component("VgPie", VgPie)
			.component("VgPieArc", VgPieArc)
	},
} as Plugin

export {
	VgGraph,
	VgGraphGrid,
	VgGraphScaleX,
	VgGraphScaleY,
	VgGraphScaleDateX,
	VgGraphScaleDateY,
	VgScaledGrid,
	VgScaledTick,
	VgScaledTicks,
	VgScaledG,
	VgScaledText,
	VgScaledValue,
	VgScaledRect,
	VgScaledForeignObject,
	VgScaledSvg,
	VgScaledCircle,
	VgScaledEllipse,
	VgScaledLine,
	VgScaledCurve,
	VgPie,
	VgPieArc,
}
