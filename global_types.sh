#!/usr/bin/env bash

# i could generate a other file (global.d.ts)
# (include file path in types property in package.json)

echo "" >> ./dist/lib.d.ts
echo "/////////// Global plugins import" >> ./dist/lib.d.ts
echo 'declare module "@vue/runtime-core" {' >> ./dist/lib.d.ts
echo '	export interface GlobalComponents {' >> ./dist/lib.d.ts

for comp_file in ./dist/components/*
do
	comp=$(basename $comp_file | cut -d. -f1)
	echo "		$comp: typeof $comp" >>  ./dist/lib.d.ts
done

echo '	}' >> ./dist/lib.d.ts
echo '}' >> ./dist/lib.d.ts
