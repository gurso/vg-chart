# Graph

## Install

    npm install @gurso/vg-chart

then in your main file :

    import { createApp } from "vue"
    import vgChart from "@gurso/vg-chart"
    import App from "./App.vue"
    ...
    const app = createApp(App)
    ...
    app.use(vgChart)
    ...
    app.mount("#app")

## Documentation

[doc(fr)](https://gurso.gitlab.io/vg-chart)

## TODO

- DOCS ticks
- DOCS min/max props
- offset start & end, rm padding ?
- allow native string attrs in props
- VgGraphGrid (think about padding inherit from VgGrph/VgScale)
- write more doc
- test on mounted components

## Add hooks

    git config core.hooksPath $PWD/hooks
