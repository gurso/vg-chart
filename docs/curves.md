<script setup>
import { ref } from "vue"
import VgGraph from "../src/components/VgGraph.vue"
import VgGraphScaleX from "../src/components/VgGraphScaleX.vue"
import VgGraphScaleY from "../src/components/VgGraphScaleY.vue"
import VgScaledCurve from "../src/components/VgScaledCurve.vue"
import VgScaledGrid from "../src/components/VgScaledGrid.vue"
import VgScaledTick from "../src/components/VgScaledTick.vue"
import { bumpFactory, basisFactory } from "../src/mods/curve"
const data = ref(Array(12).fill(100).map((v, i) => [i, Math.trunc(v * Math.random())]))
const bump = bumpFactory(0.3)
const basis = basisFactory(0.5)
</script>

# Les courbes

VgScaledCurve

## Props

| Propriétés |   Type   | Description                        |
| ---------- | :------: | ---------------------------------- |
| data       | [x, y][] | Les Donnees de la courbes          |
| type       | function | Des functions specifiques          |
| dx         |  number  | Decalage des x                     |
| dy         |  number  | Decalage des y                     |
| z          | boolean  | Relier le premier et dernier point |

Les valeurs de `x` ou `y` peuvent-etre soit un nombre, soit une date, il est necesssaire de garder une
coherence dans les valeurs. Par exemple on ne doit pas melanger des date et des nombre sur un des axes `x` ou `y`.

La propriete `type` prends une function en argument, `VgChart` fournit plusieurs fonction possibles.

## Example

Un exemple simple, l'axe des x et des y sont des nombre :

Les donnees :

```js
const data = ref(
	Array(12)
		.fill(100)
		.map((v, i) => [i, Math.trunc(v * Math.random())]),
)
```

```vue-html
<VgGraph :width="600" :height="400" :padding="[20, 40]">
    <VgGraphScaleX>
        <VgGraphScaleY reverse pretty>
            <VgScaledGrid stroke-width="1" stroke="black" />
            <VgScaledCurve :data="data" stroke-width="1px" stroke="purple" fill="none" />
        </VgGraphScaleY>
    </VgGraphScaleX>
</VgGraph>
```

<VgGraph :width="600" :height="400" :padding="[20, 40]">
    <VgGraphScaleX>
        <VgGraphScaleY reverse pretty>
            <VgScaledGrid stroke-width="1" stroke="black" />
            <VgScaledCurve :data="data" stroke-width="1px" stroke="purple" fill="none" />
        </VgGraphScaleY>
    </VgGraphScaleX>
</VgGraph>

### courbe arrondie

`vg-chart` fournit `bumpFactory` et `basisFactory` qui permetttent de generer une function a passer
en props `type` de `VgScaledCurve`.

```js
const data = ref(
	Array(12)
		.fill(100)
		.map((v, i) => [i, Math.trunc(v * Math.random())]),
)
const bump = bumpFactory(0.3)
const basis = basisFactory(0.5)
```

```vue-html
<VgGraph :width="600" :height="400" :padding="[20, 40]">
    <VgGraphScaleX>
        <VgGraphScaleY reverse pretty>
            <VgScaledGrid stroke-width="1" stroke="black" />
            <VgScaledCurve :data="data" stroke-width="1px" stroke="purple" fill="none" />
            <VgScaledCurve :data="data" :type="bump" stroke-width="1px" stroke="green" fill="none" />
            <VgScaledCurve :data="data" :type="basis" stroke-width="1px" stroke="blue" fill="none" />
        </VgGraphScaleY>
    </VgGraphScaleX>
</VgGraph>
```

<VgGraph :width="600" :height="400" :padding="[20, 40]">
    <VgGraphScaleX>
        <VgGraphScaleY reverse pretty>
            <VgScaledGrid stroke-width="1" stroke="black" />
            <VgScaledCurve :data="data" stroke-width="1px" stroke="purple" fill="none" />
            <VgScaledCurve :data="data" :type="bump" stroke-width="1px" stroke="green" fill="none" />
            <VgScaledCurve :data="data" :type="basis" stroke-width="1px" stroke="blue" fill="none" />
        </VgGraphScaleY>
    </VgGraphScaleX>
</VgGraph>
