<script setup>
import VgGraph from "../src/components/VgGraph.vue"
import VgScaledGrid from "../src/components/VgScaledGrid.vue"
import VgGraphScaleY from "../src/components/VgGraphScaleY.vue"
import VgGraphScaleX from "../src/components/VgGraphScaleX.vue"
import VgScaledCircle from "../src/components/VgScaledCircle.vue"
import VgScaledText from "../src/components/VgScaledText.vue"
import VgScaledTick from "../src/components/VgScaledTick.vue"
import VgScaledValue from "../src/components/VgScaledValue.vue"
import VgScaledTicks from "../src/components/VgScaledTicks.vue"
</script>

# Les marges

Il est plus cohérent de vouloir garder les étiquettes à l'intérieur du canvas et par conséquent de réduire
l'espace de notre graphique.

Par defaut, un axe prend comme taille la valeur du `VgGraph`, `width` pour l'axe des abscisses et `height` pour l'axe
des ordonnees.

Les composant `VgScaled[XY]` ont comme props `size`, qui permet de modifier la taille et `offset` qui decale l'axe.

```vue-html
<VgGraph :width="500" :height="300">
	<VgGraphScaleY :size="260" :offset="20">
		<VgGraphScaleX :size="460" :offset="20">
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="x" index> {{ i }} </VgScaledTick>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="y" index> {{ i }} </VgScaledTick>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="300" class="graph bd">
	<VgGraphScaleY :size="260" :offset="20">
		<VgGraphScaleX :size="460" :offset="20">
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="x" index> {{ i }} </VgScaledTick>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="y" index> {{ i }} </VgScaledTick>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

On peut aussi utiliser la propriété `padding` du composant `VgGraph`
qui prend comme valeur un tableau de nombre.
Le premier nombre défini la marge interne du haut de l'élément.
Le deuxième nombre défini la marge interne à droite de l'élément.
Le troisième nombre défini la marge interne en bas de l'élément.
Le quatrième nombre défini la marge interne à gauche de l'élément.

:::tip
L'ordre est le meme qu'en CSS. Vous pouvez utiliser 2 valeurs
pour des marges symétriques (verticales et horizontales).
Une seule valeur pour avoir la même marge des 4 quatres cotès. Dans ce cas
là, vous pouvez aussi utiliser un nombre et non un tableau.
:::

```vue-html
<VgGraph :width="500" :height="300" :padding="[20, 20, 20, 20]">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="x" index> {{ i }} </VgScaledTick>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="y" index> {{ i }} </VgScaledTick>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="300" :padding="[20,20,20,20]" class="graph bd">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="x" index> {{ i }} </VgScaledTick>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="y" index> {{ i }} </VgScaledTick>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Un problême apparait, le contour du svg ne correspond plus au graphique. Il nous appartient
d'en dessiner les bordures (voir les grilles).
