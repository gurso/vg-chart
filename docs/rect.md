<script setup>
import VgGraph from "../src/components/VgGraph.vue"
import VgScaledGrid from "../src/components/VgScaledGrid.vue"
import VgGraphScaleX from "../src/components/VgGraphScaleX.vue"
import VgGraphScaleY from "../src/components/VgGraphScaleY.vue"
import VgScaledTicks from "../src/components/VgScaledTicks.vue"
import VgScaledValue from "../src/components/VgScaledValue.vue"
import VgScaledCircle from "../src/components/VgScaledCircle.vue"
import VgScaledRect from "../src/components/VgScaledRect.vue"
const data = Array.from({length: 10}, () => Math.random() * 100)
</script>

# Les rectangles

L'element `VgScaledRect` permet de creer des rectangle.

## props

| Propriétés |     Type     | Description                          |
| ---------- | :----------: | ------------------------------------ |
| x          | number\|Date | la valeur du cotés gauche            |
| y          | number\|Date | la valeur du haut du rectangle       |
| x2         | number\|Date | la valeur du cotés droit             |
| y2         | number\|Date | la valeur du bas du rectangle        |
| w          |    number    | la valeur de la largeur du rectangle |
| h          |    number    | la valeur de la hauteur du rectangle |
| dx         |    number    | un décalage sur l'axe des x          |
| dy         |    number    | un décalage sur l'axe des y          |

Les propriétées `x2` et `y2` permettent de positionné un rectangle par rapport
au cotés opposé habituellement.

Pour être fonctionnel, un rectangle à besoin d'aux moins 2 valeurs sur chaque axes.  
2 valeurs parmis x, x2, w et width pour l'axe des x.  
2 valeurs parmis y, y2, h et height pour l'axe des y.

<VgGraph :width="600" :height="300" :padding="40" class="graph">
	<VgGraphScaleX>
		<VgGraphScaleY reverse>
			<VgScaledRect v-for="(v, i) of data" :x="i" :y="0" :y2="v" width="32" fill="purple" />
		</VgGraphScaleY>
	</VgGraphScaleX>
</VgGraph>

## ForeignObject

La logique de `VgScaledRect` est la meme pour les composant `VgGraphForeignObject`
(avec la possibilite d'ecrire du html a l'interieur) et `VgScaledSvg`.
