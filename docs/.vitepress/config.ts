import { defineConfig } from "vitepress"

export default defineConfig({
	base: "/vg-chart/",
	title: "Vg chart",
	description: "Build your chart",
	lang: "fr-FR",
	outDir: "dist",
	themeConfig: {
		nav: [],
		sidebar: [
			{
				text: "Les concepts",
				collapsed: false,
				items: [
					{ text: "Introduction", link: "/" },
					{ text: "Les axes", link: "/axis" },
					{ text: "Les étiquettes", link: "/ticks" },
					{ text: "Les marges", link: "/margins" },
					{ text: "Les grilles", link: "/grids" },
					{ text: "Les multiples", link: "/multiples" },
					{ text: "Les dates", link: "/dates" },
					{ text: "Les graphiques circulaires", link: "/pie" },
				],
			},
			{
				text: "Les composants",
				collapsed: false,
				items: [
					{ text: "Les rectangles", link: "/rect" },
					{ text: "Les courbes", link: "/curves" },
				],
			},
			{
				text: "Exemples",
				collapsed: false,
				items: [{ text: "", link: "" }],
			},
		],
	},
})
