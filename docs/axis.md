<script setup>
import VgGraph from "../src/components/VgGraph.vue"
import VgGraphScaleX from "../src/components/VgGraphScaleX.vue"
import VgGraphScaleY from "../src/components/VgGraphScaleY.vue"
import VgScaledCircle from "../src/components/VgScaledCircle.vue"
import VgScaledValue from "../src/components/VgScaledValue.vue"
</script>

# Comprendre les échelles

Pour commencer, partons sur un exemple simple comme un classique nuage de points.
Chaque point posséde une valeur x et une valeur y qui
vont déterminer sa position dans le graphique.

Dans les exemples ci-dessous, une bordure CSS est ajouté à l'élément `<svg>`.

```css
svg {
	outline: solid black 1px;
}
```

## Créer un graphique

Pour créer un graphique, il faut utiliser le composant `VgGraph`,
2 propriétées sont requises pour cette attribut, `width` et `height`
qui prennent comme valeur le nombre de pixels.

```vue-html
<VgGraph :width="500" :height="200"></VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd"></VgGraph>

Si l'on inspecte le DOM, on peut constater qu'un élément svg à été
créé avec les valeurs des attributs associés.

L'élément `VgGraph` ne suffit pas pour créer un simple graphiste.
Il faut aussi définir au moins une échelle pour l'axe des x, et une autre pour l'axe des y.
On peut le réaliser grâce aux composants `VgGraphScaleX` et `VgGraphScaleY`.

```vue-html
<VgGraph :width="200" :height="200">
	<VgGraphScaleY>
		<VgGraphScaleX>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

::: tip
à noter que l'on peut inverser l'ordre et mettre `VgGraphScaleY` à l'intérieur d'un `VgGraphScaleX`.
:::

Pour l'instant, le dom généré reste le même, aucun élément n'a été rajouté au dom.
Commencons par ajouter un point, pour se faire il faut utiliser le composant `VgScaledCircle`.
Ce composant a besoin au minimum de 2 props, `cx` et `cy` qui
sont les valeurs du point dans le graphique.
Pour que le point soit visible, il faut lui donné une taille et
lui ajouté un peu de style.
`VgScaledCircle` peut prendre en attribut n'importe quel
attribut de l'élément `circle` de la norme SVG.
On utilise donc `r` pour le rayon (en pixel) et `fill` pour la couleur de remplissage.

```vue-html{4}
<VgGraph :width="500" :height="200">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledCircle :cx="200" :cy="100" r="5" fill="red" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledCircle :cx="200" :cy="100" r="5" fill="red" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Le résultat peut parêtre inattendu, le point se situe au centre.

Il faut bien comprendre une chose, la position du point va être calculer en
fonction de données récupérées pas toutes les valeurs des composant à l'intérieur
d'un axis (`VgGraphScaleX` ou `VgGraphScaleY`).

En gros des produits en croix sont réalisées avec les valeurs fournies et les valeurs
minimales et maximales d'un jeu de données.

et si il n'y a qu'une seule données, elle est centrée.

Essayons donc avec un jeu de données plus conséquent :

```vue-html{4-8}
<VgGraph :width="500" :height="200">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledCircle :cx="10" :cy="10" r="5" fill="red" />
			<VgScaledCircle :cx="20" :cy="60" r="5" fill="green" />
			<VgScaledCircle :cx="30" :cy="30" r="5" fill="blue" />
			<VgScaledCircle :cx="40" :cy="90" r="5" fill="pink" />
			<VgScaledCircle :cx="50" :cy="80" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledCircle :cx="10" :cy="10" r="5" fill="red" />
			<VgScaledCircle :cx="20" :cy="60" r="5" fill="green" />
			<VgScaledCircle :cx="30" :cy="30" r="5" fill="blue" />
			<VgScaledCircle :cx="40" :cy="90" r="5" fill="pink" />
			<VgScaledCircle :cx="50" :cy="80" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

On progresse, on voit donc que les points se placent automatiquement sur le graphique.
Notons que le point rouge se situe dans le coin en haut à gauche du graphique.
C'est le cas car il posséde les plus petites valeurs
sur l'axe des x et sur l'axe des y.  
Le point rose à la plus grande valeur sur l'axe des y, il est donc situé à l'extremité
du graphique en bas.  
Le point violet à la plus grande valeur sur l'axe des x, il est donc situé à l'extremité
du graphique à droite.

Imaginons que les valeurs sur l'axe des y soient des pourcentages.
Nous savons donc que la plus petite valeur est 0 et que la plus grande et 100.
Nous n'allons pas ajouter 2 `VgScaledCircle` pour définir ces valeurs. On peut utiliser
le composant `VgScaledValue` qui ne déssine rien mais ajoute sa valeur à
un jeu de donnée d'échelle.

```vue-html{4,5}
<VgGraph :width="500" :height="200">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledValue :add="0" axis="y" />
			<VgScaledValue :add="100" axis="y" />
			<VgScaledCircle :cx="10" :cy="10" r="5" fill="red" />
			<VgScaledCircle :cx="20" :cy="60" r="5" fill="green" />
			<VgScaledCircle :cx="30" :cy="30" r="5" fill="blue" />
			<VgScaledCircle :cx="40" :cy="90" r="5" fill="pink" />
			<VgScaledCircle :cx="50" :cy="80" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledValue :add="0" axis="y" />
			<VgScaledValue :add="100" axis="y" />
			<VgScaledCircle :cx="10" :cy="10" r="5" fill="red" />
			<VgScaledCircle :cx="20" :cy="60" r="5" fill="green" />
			<VgScaledCircle :cx="30" :cy="30" r="5" fill="blue" />
			<VgScaledCircle :cx="40" :cy="90" r="5" fill="pink" />
			<VgScaledCircle :cx="50" :cy="80" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Notez qu'il faut préciser la propriété axis pour dire sur quel axe sont ajoutée
les valeurs.
disons maintenant que les valeurs de l'axe des abscisses soit compris
entre 0 et 60 :

```vue-html{6,7}
<VgGraph :width="500" :height="200">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="100" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="60" />
			<VgScaledCircle :cx="10" :cy="10" r="5" fill="red" />
			<VgScaledCircle :cx="20" :cy="60" r="5" fill="green" />
			<VgScaledCircle :cx="30" :cy="30" r="5" fill="blue" />
			<VgScaledCircle :cx="40" :cy="90" r="5" fill="pink" />
			<VgScaledCircle :cx="50" :cy="80" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="100" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="60" />
			<VgScaledCircle :cx="10" :cy="10" r="5" fill="red" />
			<VgScaledCircle :cx="20" :cy="60" r="5" fill="green" />
			<VgScaledCircle :cx="30" :cy="30" r="5" fill="blue" />
			<VgScaledCircle :cx="40" :cy="90" r="5" fill="pink" />
			<VgScaledCircle :cx="50" :cy="80" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Plusieurs remarques, les composants `VgScaledValue` n'ont pas de propriété axis.
En effet, le composant `VgScaledValue` utilise par default l'échelle du composant axis
ancêtre le plus proche. Les 2 premiers avec les valeurs 0 et 100 ajoute donc leurs
valeur au axis des y, et les 2 suivant avec les valeurs 0 et 60 au axis des x.

On peut utiliser la propriété axis pour être explicite et/ou mettre les composant `VgScaledValue`
n'importe ou a l'intérieur du composant `VgScaled[XY]` le plus profond.

Imaginons maintenant que nous ayons certaines valeurs négative ou que certaines
dépassent 100. Le graph va inclure ses valeurs dans son index et les prendre en compte
pour l'affichage. Un cercle avec la valeur `cy` a 0 ne sera plus tout en haut
du graphique et pareillement la valeur de 100 tout en bas du graphique.

```vue-html
<VgGraph :width="500" :height="200">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="100" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="60" />
			<VgScaledCircle :cx="10" :cy="-23" r="5" fill="red" />
			<VgScaledCircle :cx="20" :cy="0" r="5" fill="pink" />
			<VgScaledCircle :cx="30" :cy="100" r="5" fill="purple" />
			<VgScaledCircle :cx="40" :cy="117" r="5" fill="green" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="100" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="60" />
			<VgScaledCircle :cx="10" :cy="-23" r="5" fill="red" />
			<VgScaledCircle :cx="20" :cy="0" r="5" fill="pink" />
			<VgScaledCircle :cx="30" :cy="100" r="5" fill="purple" />
			<VgScaledCircle :cx="40" :cy="117" r="5" fill="green" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Ce sont maintenant les points rouge et vert qui sont aux extremités. Le point rouge
a pour valeur -23, qui est la valeur minimal et le point vert qui a la plus
grandes des valeurs (117).

Si l'on ne souhaite pas que des valeurs soient indexées,
on utilise la propriété `ignore`.

```vue-html{8-14}
<VgGraph :width="500" :height="200">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="100" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="60" />
			<VgScaledCircle :cx="10" :cy="-23" r="5" fill="black" ignore />
			<VgScaledCircle :cx="10" :cy="23" r="5" fill="blue" ignore />
			<VgScaledCircle :cx="20" :cy="0" r="5" fill="red" ignore />
			<VgScaledCircle :cx="25" :cy="50" r="5" fill="green" ignore />
			<VgScaledCircle :cx="30" :cy="100" r="5" fill="purple" ignore />
			<VgScaledCircle :cx="40" :cy="117" r="5" fill="black" ignore />
			<VgScaledCircle :cx="50" :cy="153" r="5" fill="black" ignore />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="100" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="60" />
			<VgScaledCircle :cx="10" :cy="-23" r="5" fill="black" ignore />
			<VgScaledCircle :cx="10" :cy="23" r="5" fill="blue" ignore />
			<VgScaledCircle :cx="20" :cy="0" r="5" fill="red" ignore />
			<VgScaledCircle :cx="25" :cy="50" r="5" fill="green" ignore />
			<VgScaledCircle :cx="30" :cy="100" r="5" fill="purple" ignore />
			<VgScaledCircle :cx="40" :cy="117" r="5" fill="black" ignore />
			<VgScaledCircle :cx="50" :cy="153" r="5" fill="black" ignore />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Les points ignorés avec des valeurs supérieures ou inférieures à l'ensemble des valeurs
enregistrées ne sont pas visibles car en dehors de la partie visible du svg.
On peut voir qu'ils sont bien presents avec un peu de CSS :

```css
svg {
	overflow: visible;
}
```

Quelques remarques :

On peut bien sur utiliser la directive `v-for` afin d'afficher des jeux de données.  
Jusqu'ici, les exemple sont données du haut vers le bas comme un élément svg.
On peux inverser le sens d'un axe d'échelle `VgScaled[XY]` avec la propriété `reverse`.

```vue-html{2,4}
<VgGraph :width="500" :height="200">
	<VgGraphScaleY reverse>
		<VgGraphScaleX>
			<VgScaledCircle v-for="i of 10" :key="i" :cx="i" :cy="i * 10" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY reverse>
		<VgGraphScaleX>
			<VgScaledCircle v-for="i of 10" :key="i" :cx="i" :cy="i * 10" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

::: warning
Pour rappel, `v-for="i of 10"`, `i` commence à 1 et non 0.
:::

Enfin, plutot que de laisser les échelles se calculent toutes seules, on peut forcer les valeurs minimum et maximum.

```vue-html{2}
<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY :min="0" :max="200" reverse>
		<VgGraphScaleX>
			<VgScaledCircle v-for="i of 10" :key="i" :cx="i" :cy="i * 10" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY :min="0" :max="50" reverse>
		<VgGraphScaleX>
			<VgScaledCircle v-for="i of 10" :key="i" :cx="i" :cy="i * 10" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
