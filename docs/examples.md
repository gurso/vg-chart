<script setup>
import VgGraph from "../src/components/VgGraph.vue"
import VgScaledGrid from "../src/components/VgScaledGrid.vue"
import VgGraphScaleDateX from "../src/components/VgGraphScaleDateX.vue"
import VgGraphScaleY from "../src/components/VgGraphScaleY.vue"
import VgScaledTicks from "../src/components/VgScaledTicks.vue"
import VgScaledValue from "../src/components/VgScaledValue.vue"
import VgScaledCircle from "../src/components/VgScaledCircle.vue"
import VgScaledRect from "../src/components/VgScaledRect.vue"
import VgScaledCurve from "../src/components/VgScaledCurve.vue"
const y = new Date().getFullYear()
const nextYear = new Date(y + 1, 0)
const data = Array.from({length: 12}, (v, i) => [new Date(y, i), Math.random() * 100])
const data2 = Array.from({length: 12}, (v, i) => [new Date(y, i), Math.random() * 10])
function dateFormat(date) {
  return ["jan","fev","mars","avr","mai","juin","juil","aout","sept","oct","nov","dec"][date.getMonth()]
}
</script>

# Exemples

## 2 axes des Y

<VgGraph :width="700" :height="300" :padding="40">
    <VgGraphScaleDateX :size="500" :offset="60">
        <VgScaledValue :add="nextYear" />
        <VgGraphScaleY reverse :multiples="10">
            <VgScaledGrid stroke="black" :y="5" :x="0" stroke-width="1" />
            <VgScaledValue :add="0" />
            <!-- <VgScaledRect v-for="v of data" :x="v[0]" :y="0" :y2="v[1]" width="32" fill="purple" /> -->
            <VgScaledTicks axis="x" :dx="16" :format="dateFormat" :dy="8" :m="1" :limit="12" bottom />
            <VgScaledTicks axis="y" :nb="5" dx="-8" />
        </VgGraphScaleY>
        <VgGraphScaleY reverse :multiples="10">
            <VgScaledTicks axis="y" :nb="3" dx="8" right />
            <VgScaledCircle v-for="v of data2" :cx="v[0]" :dx="16" :cy="v[1]" r="4" fill="red" />
            <VgScaledCurve :data="data2" :dx="16" stroke="red" stroke-width="2" fill="transparent" />
        </VgGraphScaleY>
    </VgGraphScaleDateX>
</VgGraph>
