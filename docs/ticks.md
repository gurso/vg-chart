<script setup>
import VgGraph from "../src/components/VgGraph.vue"
import VgGraphScaleY from "../src/components/VgGraphScaleY.vue"
import VgGraphScaleX from "../src/components/VgGraphScaleX.vue"
import VgScaledCircle from "../src/components/VgScaledCircle.vue"
import VgScaledText from "../src/components/VgScaledText.vue"
import VgScaledTick from "../src/components/VgScaledTick.vue"
import VgScaledValue from "../src/components/VgScaledValue.vue"
import VgScaledTicks from "../src/components/VgScaledTicks.vue"
</script>

# Les étiquettes

## VgScaledText

Au même titre que l'élément circle, est fournit un équivalent à l'élément `<text>`
en svg.

le composant `VgScaledText` fournie 3 propriétés :

- `x` (number) qui définit la position de l'élément sur l'axe des x.
- `y` (number) qui définit la position de l'élément sur l'axe des y.
- `ignore` qui évite que les valeurs de `x` et`y` soit enregistré au sein de leur index respectif.

On peut aussi lui passer n'importe quel attributs disponibles sur un élément `<text>` de la norme svg.

Premier exemple avec des tooltips très basiques :

```vue-html
<VgGraph :width="500" :height="200">
	<VgGraphScaleY reverse>
		<VgGraphScaleX>
			<VgScaledCircle v-for="i in 10" :key="i" :cx="i" :cy="i" r="5" fill="purple" />
			<VgScaledText v-for="i in 10" :key="i" :x="i" :y="i"> {{ i }} test </VgScaledText>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY reverse>
		<VgGraphScaleX>
			<VgScaledCircle v-for="i in 10" :key="i" :cx="i" :cy="i" r="5" fill="purple" />
			<VgScaledText v-for="i in 10" :key="i" :x="i" :y="i"> {{ i }} test </VgScaledText>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

L'élément natif `<text>` fournit 2 attributs `dx` et `dy` qui permettent d'appliquer un décalage relatif.

```vue-html
<VgGraph :width="500" :height="200">
	<VgGraphScaleY reverse>
		<VgGraphScaleX>
			<VgScaledCircle v-for="i in 10" :key="i" :cx="i" :cy="i" r="5" fill="purple" />
			<VgScaledText v-for="i in 10" :key="i" :x="i" :y="i" dy="-5" dx="5"> {{ i }} test </VgScaledText>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" class="graph bd">
	<VgGraphScaleY reverse>
		<VgGraphScaleX>
			<VgScaledCircle v-for="i in 10" :key="i" :cx="i" :cy="i" r="5" fill="purple" />
			<VgScaledText v-for="i in 10" :key="i" :x="i" :y="i" dy="-5" dx="5"> {{ i }} test </VgScaledText>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

## VgScaledTick

Il est courant d'inclure dans un graphique des labels qui informent sur les valeurs contenues dans celui-ci.

Le composant `VgScaledTick` créé un élément `text` positioné au extrémités du graphique.

Contrairement aux composants `VgScaledText` et `VgScaledCircle`, `VgScaledTick`
n'indexe pas par défaut sa valeur, elle n'aura aucun impacte sur l'échelle du graphique.

Le composant `VgScaledTick` prends les propriété suivantes :

- `value` (number|Date) qui va définir sa position sur son axe.
- `axis` (`x`|`y`) définit sur quel axe il est indexé et placé.
- `index` (boolean) pour que sa valeur soit indexée.
- `bottom` (boolean) si le axis définit est `x`, place l'étiquette en bas du graphique
- `right` (boolean) si le axis définit est `y`, place l'étiquette à droite du graphique

Tous les attributs supplémentaire sont ajoutés à l'élément `<text>` généré.

```vue-html
<VgGraph :width="500" :height="200">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="x" index> {{ i }} </VgScaledTick>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="y" index> {{ i }} </VgScaledTick>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

Avec ce code les étiquettes vont être placées en dehors du canvas visible du svg.
Pour pouvoir les voir, il faut modifier l'overflow de l'élément svg :

```css
svg {
	overflow: visible;
}
```

<VgGraph :width="500" :height="200" class="graph bd ov">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="x" index> {{ i }} </VgScaledTick>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="y" index> {{ i }} </VgScaledTick>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Les étiquettes sont collées au canvas, on peut utiliser les attributs `dx` et `dy`
pour les espacer.

### VgScaledTicks

Le composant `VgScaledTicks` (avec un "s") permet de générer plusieurs étiquettes en fonction des données
des composants qui index leur données.
`VgScaledTicks` a besoin de connaitre le nombre d'etiquettes voulues via la props `nb`.
Il fournit ensuite via `v-slot` un tableaux de nombre que l'on peut afficher avec `VgScaledTick`.

```vue-html
<VgGraph :width="600" :height="200" class="graph bd ov">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="10" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="10" />
			<VgScaledTicks :nb="4" v-slot="{ ticks }">
                <VgScaledTick v-for="tick of ticks" axis="x" dy="-5" :value="tick">{{ tick }}</VgScaledTick>
            </VgScaledTicks>
			<VgScaledTicks :nb="3" v-slot="{ ticks }">
                <VgScaledTick v-for="tick of ticks" axis="y" dx="-5" :value="tick">{{ tick }}</VgScaledTick>
            </VgScaledTicks>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="600" :height="200" class="graph bd ov">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="10" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="10" />
			<VgScaledTicks :nb="4" v-slot="{ ticks }">
                <VgScaledTick v-for="tick of ticks" axis="x" dy="-5" :value="tick">{{ tick }}</VgScaledTick>
            </VgScaledTicks>
			<VgScaledTicks :nb="3" v-slot="{ ticks }">
                <VgScaledTick v-for="tick of ticks" axis="y" dx="-5" :value="tick">{{ tick }}</VgScaledTick>
            </VgScaledTicks>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Sans slot, `VgScaledTicks` affiche directement les valeurs calculees.

```vue-html{8,9}
<VgGraph :width="600" :height="200" class="graph bd ov">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="10" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="10" />
			<VgScaledTicks :nb="4" />
			<VgScaledTicks :nb="3" axis="y" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="600" :height="200" class="graph bd ov">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="10" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="10" />
			<VgScaledTicks :nb="4" />
			<VgScaledTicks :nb="3" axis="y" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Pour modifier l'affichage, on peut utiliser la propriété `format` qui prend une fonction en valeur.

#### pretty

On peut demander à `VgScaledTicks` de choisir les étiquettes les plus "significatives", elles ne seront alors
plus forcément aux extremités.

```vue-html{8,9}
<VgGraph :width="600" :height="200" class="graph bd ov">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="1000" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="1000" />
			<VgScaledTicks :nb="4" pretty dy="-10" />
			<VgScaledTicks :nb="3" axis="y" pretty dx="-10" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="600" :height="200" class="graph bd ov">
	<VgGraphScaleY>
		<VgScaledValue :add="0" />
		<VgScaledValue :add="1000" />
		<VgGraphScaleX>
			<VgScaledValue :add="0" />
			<VgScaledValue :add="1000" />
			<VgScaledTicks :nb="4" pretty dy="-10" />
			<VgScaledTicks :nb="3" axis="y" pretty dx="-10" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

Un autres solutions est d'adapter les valeurs des axes. Voir les [multiples](/multiples.html).
