<script setup lang="ts">
import VgPie from "../src/components/VgPie.vue"
import VgPieArc from "../src/components/VgPieArc.vue"
</script>

# Graphiques circulaires

Par defaut, la repartition totale du graphique vaut 100.
on peut la midifier avec les props `min` et `max`

```vue-html
<VgPie :size="200">
    <VgPieArc :start="0" :end="20" fill="blue" />
    <VgPieArc :start="20" :end="50" fill="red" />
    <VgPieArc :start="50" :end="60" fill="green" />
    <VgPieArc :start="60" :end="100" fill="purple" />
</VgPie>
```

<VgPie :size="200">
    <VgPieArc :start="0" :end="20" fill="blue" />
    <VgPieArc :start="20" :end="50" fill="red" />
    <VgPieArc :start="50" :end="60" fill="green" />
    <VgPieArc :start="60" :end="100" fill="purple" />
</VgPie>

## hole

La valeur de `hole` est comprise entre 0 et 1 et represente la proprtion de la taille du trou.
Par defaut, `hole` vaut 0.

<VgPie :size="200" :hole="0.8">
    <VgPieArc :start="0" :end="20" fill="blue" />
    <VgPieArc :start="20" :end="50" fill="red" />
    <VgPieArc :start="50" :end="60" fill="green" />
    <VgPieArc :start="60" :end="100" fill="purple" />
</VgPie>

Chaque `VgPieArc` peut avoir un `hole` qui lui est propre.

<VgPie :size="200" :hole="0.4">
    <VgPieArc :start="0" :end="20" :hole="0.5" fill="blue" />
    <VgPieArc :start="20" :end="50" :hole="0.4" fill="red" />
    <VgPieArc :start="50" :end="60" :hole="0.6" fill="green" />
    <VgPieArc :start="60" :end="100" fill="purple" />
</VgPie>

## ply

`ply` pour definir l'epaisseur fonctionne comme `hole`. Si hole est fournit, `ply` est automatiquement egale
a `1 - hole`.

<VgPie :size="200">
    <VgPieArc :start="0" :end="20" :ply="0.5" fill="blue" />
    <VgPieArc :start="20" :end="50" :ply="0.4" fill="red" />
    <VgPieArc :start="50" :end="60" :ply="0.6" fill="green" />
    <VgPieArc :start="60" :end="100" fill="purple" />
</VgPie>

<VgPie :size="200" :hole="0.4">
    <VgPieArc :start="0" :end="20" :ply="0.5" fill="blue" />
    <VgPieArc :start="20" :end="50" :ply="0.4" fill="red" />
    <VgPieArc :start="50" :end="60" :ply="0.6" fill="green" />
    <VgPieArc :start="60" :end="100" fill="purple" />
</VgPie>

## gap

<VgPie :size="200" :gap="12" :hole="0.7">
    <VgPieArc :start="0" :end="20" fill="blue" />
    <VgPieArc :start="20" :end="50" fill="red" />
    <VgPieArc :start="50" :end="60" fill="green" />
    <VgPieArc :start="60" :end="100" fill="purple" />
</VgPie>

## turn

<VgPie :size="200" :turn="0">
    <VgPieArc :start="0" :end="50" fill="blue" />
    <VgPieArc :start="50" :end="100" fill="purple" />
</VgPie>
