<script setup>
import VgGraph from "../src/components/VgGraph.vue"
import VgScaledGrid from "../src/components/VgScaledGrid.vue"
import VgGraphScaleY from "../src/components/VgGraphScaleY.vue"
import VgGraphScaleX from "../src/components/VgGraphScaleX.vue"
import VgScaledCircle from "../src/components/VgScaledCircle.vue"
import VgScaledText from "../src/components/VgScaledText.vue"
import VgScaledTick from "../src/components/VgScaledTick.vue"
import VgScaledValue from "../src/components/VgScaledValue.vue"
import VgScaledTicks from "../src/components/VgScaledTicks.vue"
</script>
<style>
svg {overflow: visible}
</style>

# Les grilles

## VgScaledGrid

Le composant `VgScaledGrid` permet de dessiner des ... grilles. Le composant prend en paramètre
2 propriétés. Le nombre de lignes horizontales avec la propriété `y` et le nombre de ligne verticale
avec `x`. par défaut la valeur de ces propriétés est 2. Ce qui a pour effet de dessiner les contour des axes
dans lequel se situe le composant.

On peut lui passé les même attributs que l'élément `line` en svg afin de définir
le style de l'ensemble des lignes :

```vue-html{4}
<VgGraph :width="500" :height="200" :padding="[20, 5, 5, 20]">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledTick v-for="i in 5" :key="i" :value="i"> {{ i }} </VgScaledTick>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="y"> {{ i }} </VgScaledTick>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="600" :height="250" :padding="[20, 5, 5, 20]" class="graph bd">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledTick v-for="i in 5" :key="i" :value="i" index> {{ i }} </VgScaledTick>
			<VgScaledTick v-for="i in 5" :key="i" :value="i" axis="y" index> {{ i }} </VgScaledTick>
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

A partir d'ici, plus besoin de dessiner les bordures CSS de `<svg>`.

Si on veut garder la meme taille qu'avant, il faut aussi agrandir les propriétés `width` et `height` de notre `VgGraph`.
