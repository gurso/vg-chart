# VgGraph

Cette librairie se concentre exclusivement sur des graphiques dans un répère orthogonal,
avec minimum un axe des x et un axe des y.

Cette librairie ne fournie pas de composants graphiques déjà prêts à l'emploi mais
plutôt un ensemble de composants qui permet de
créer ses propres graphiques en facilitant certaines tâches fastidieuses.

Plusieurs composants sont une surcouche aux éléments natifs du svg. @gurso/vg-chart facilite la gestion
des coordonnées sur un graphique mais ne fournit aucune aide pour le style de ces composants.

## Installation

```sh
npm install @gurso/vg-chart
```

Il est possible d'importer tous les composants globalement :

```js
import { createApp } from "vue"
import vGraph from "@gurso/vg-chart"
import App from "./App.vue"
...
const app = createApp(App)
...
app.use(vGraph)
...
app.mount("#app")
```

ou d'importer les composants dans chaque fichier où ils sont utilisés.
