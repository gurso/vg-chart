<script setup>
import VgGraph from "../src/components/VgGraph.vue"
import VgScaledGrid from "../src/components/VgScaledGrid.vue"
import VgGraphScaleDateX from "../src/components/VgGraphScaleDateX.vue"
import VgGraphScaleY from "../src/components/VgGraphScaleY.vue"
import VgScaledTicks from "../src/components/VgScaledTicks.vue"
import VgScaledCircle from "../src/components/VgScaledCircle.vue"
import VgScaledValue from "../src/components/VgScaledValue.vue"
const y = new Date().getFullYear()
const data = Array.from({length: 12}, (v, i) => ({date: new Date(y, i), value: i}))
function dateFormat(date) {
	return ["jan","fev","mars","avr","mai","juin","juillet","aout","sept","oct","nov","dec"][date.getMonth()]
}
</script>

# Les dates

Jusqu'à présent les exemples utilisaient des nombres. Il est courant pour des graphiques
de représenter une ligne temporelle.

Les composants `VgGraphScaleX` et `VgGraphScaleY` qui gérent des nombres ont
leurs équivalents `VgGraphScaleDateX` et `VgGraphScaleDateY` pour gérer les dates.
Le concept est le même, la différence est que les valeurs relatives à un axe de date
doivent être des dates.

Un exemple sera plus parlant, avec un simple jeu de données où l'on imagine une valeur par mois:

```js
const y = new Date().getFullYear()
const data = Array.from({ length: 12 }, (v, i) => ({ date: new Date(y, i), value: i }))
```

Qu'on affiche avec des points :

```vue-html
<VgGraph :width="600" :height="300">
	<VgGraphScaleDateX>
		<VgGraphScaleY reverse>
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="d.date" :cy="d.value" r="5" fill="purple" />
		</VgGraphScaleY>
	</VgGraphScaleDateX>
</VgGraph>
```

Résultat :

<VgGraph :width="600" :height="300" class="graph">
	<VgGraphScaleDateX>
		<VgGraphScaleY reverse>
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="d.date" :cy="d.value" r="5" fill="purple" />
		</VgGraphScaleY>
	</VgGraphScaleDateX>
</VgGraph>

:::warning
Les axis de date n'ont pas la propriété `multiples`.
:::

## Les étiquettes

Ajoutons des étiquettes au graphique ci-dessus, avec une marge et des repères.

Ajoutons aussi une fonction pour afficher la date des étiquettes dans un format plus clair.

```js
function dateFormat(date) {
	return ["jan", "fev", "mars", "avr", "mai", "juin", "juillet", "aout", "sept", "oct", "nov", "dec"][date.getMonth()]
}
```

```vue-html
<VgGraph :width="600" :height="300" :padding="40">
	<VgGraphScaleDateX>
		<VgGraphScaleY reverse>
			<VgScaledTicks axis="x" :format="dateFormat" :dy="8" bottom />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="d.date" :cy="d.value" r="5" fill="purple" />
		</VgGraphScaleY>
	</VgGraphScaleDateX>
</VgGraph>
```

Résultat :

<VgGraph :width="600" :height="300" :padding="40" class="graph">
	<VgGraphScaleDateX>
		<VgGraphScaleY reverse>
			<VgScaledTicks axis="x" :format="dateFormat" :dy="8" bottom />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :cx="d.date" :cy="d.value" r="5" fill="purple" />
		</VgGraphScaleY>
	</VgGraphScaleDateX>
</VgGraph>

Dans ce genre de graphique, il n'est pas rare d'afficher une étiquettes par mois.
On connait la propriété `nb` de `VgScaledTicks`, mais cette propriété n'est disponible que
lorsque que l'échelle est une échelle de nombre.
De plus celle-ci se contente de diviser en part égale,
les dates affichées ne seraient donc pas le premier du mois
vu que ceux-ci n'ont pas le même nombre de jours.

`VgScaledTicks` propose d'autre propriétés liées aux dates.

- year: nombre d'années
- month: nombre de mois
- day: nombre de jours
- hour: nombre d'heures
- minute: nombre de minutes
- second: nombre de secondes
- millisecond: nombre millisecondes

`VgScaledTicks` fait le cumule des valeurs pour définir l'écart entre les étiquettes.
Par exemple, avec les valeurs hour = 3 et minute = 15, une étiquettes sera affichée toutes
les 3 heure et 15 minutes.  
Pour afficher chaque mois il suffit de mettre 1 comme valeur a month.

```vue-html
<VgGraph :width="600" :height="300" :padding="40">
	<VgGraphScaleDateX>
		<VgGraphScaleY reverse>
			<VgScaledTicks axis="x" :format="dateFormat" :dy="8" bottom :month="1" />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="d.date" :cy="d.value" r="5" fill="purple" />
		</VgGraphScaleY>
	</VgGraphScaleDateX>
</VgGraph>
```

<VgGraph :width="600" :height="300" :padding="40" class="graph">
	<VgGraphScaleDateX>
		<VgGraphScaleY reverse>
			<VgScaledTicks axis="x" :format="dateFormat" :dy="8" bottom :month="1" />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="d.date" :cy="d.value" r="5" fill="purple" />
		</VgGraphScaleY>
	</VgGraphScaleDateX>
</VgGraph>
